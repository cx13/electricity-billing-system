package electricity;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class About extends JFrame implements ActionListener {

    JButton exitBtn;
    JLabel label;
    Font textFont,labelFont,f2;

    TextArea textArea;
    String s;


    public About(){
        exitBtn=new JButton("Exit");
        add(exitBtn);
        exitBtn.setBounds(180,430,120,20);
        exitBtn.addActionListener(this);

        s = "                      About Projects          \n  "
                + "\nElectricity Billing System is a software-based application "
                + "developed in Java programming language. The project aims at serving"
                + "the department of electricity by computerizing the billing system. "
                + "It mainly focuses on the calculation of Units consumed during the "
                + "specified time and the money to be paid to electricity offices. "
                + "This computerized system will make the overall billing system easy, "
                + "accessible, comfortable and effective for consumers.\n\n"
        ;

        textArea =new TextArea(s,10,40,Scrollbar.VERTICAL);
        textArea.setEditable(false);
        textArea.setBounds(20,100,450,300);
        add(textArea);


        textFont = new Font("RALEWAY", Font.BOLD, 16);
        textArea.setFont(textFont);

        label=new JLabel("About Project");
        add(label);
        label.setBounds(170,10,180,80);
        label.setForeground(Color.RED);

        labelFont=new Font("RALEWAY",Font.BOLD,20);
        label.setFont(labelFont);

        setBounds(700,220,500,550);

        setLayout(null);
    }




    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        dispose();

    }

    public static void main(String[] args) {
        new About().setVisible(true);
    }
}
