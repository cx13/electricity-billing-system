package electricity;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Objects;

public class Login extends JFrame implements ActionListener {

    JButton loginBtn,cancelBtn,signupBtn;
    JLabel usernameL,passwordL,loginAsL,frameL;
    JTextField usernameJTF;
    JPasswordField passwordJF;

    Choice loggingChoice;


    public Login(){
        setLayout(null);
        getContentPane().setBackground(Color.WHITE);

        ImageIcon ic1 = new ImageIcon(ClassLoader.getSystemResource("icon/login.png"));
        Image i1 = ic1.getImage().getScaledInstance(16, 16,Image.SCALE_DEFAULT);
        loginBtn =new JButton("Login", new ImageIcon(i1));
        loginBtn.setBounds(330, 160, 100, 20);
        ImageIcon ic2 = new ImageIcon(ClassLoader.getSystemResource("icon/cancel.jpg"));
        Image i2 = ic2.getImage().getScaledInstance(16, 16,Image.SCALE_DEFAULT);
        cancelBtn =new JButton("Cancel",new ImageIcon(i2));
        cancelBtn.setBounds(450, 160, 120, 20);
        ImageIcon ic4 = new ImageIcon(ClassLoader.getSystemResource("icon/signup.png"));
        Image i4 = ic4.getImage().getScaledInstance(16, 16,Image.SCALE_DEFAULT);
        signupBtn =new JButton("Signup",new ImageIcon(i4));
        signupBtn.setBounds(380, 200, 130, 20);
        add(loginBtn);
        add(cancelBtn);
        add(signupBtn);
        loginBtn.addActionListener(this);
        cancelBtn.addActionListener(this);
        signupBtn.addActionListener(this);

        ImageIcon ic3 = new ImageIcon(ClassLoader.getSystemResource("icon/second.jpg"));
        Image i3 = ic3.getImage().getScaledInstance(250, 250,Image.SCALE_DEFAULT);
        ImageIcon icc3 = new ImageIcon(i3);
        frameL = new JLabel(icc3);
        frameL.setBounds(0, 0, 250, 250);
        add(frameL);

        usernameL=new JLabel("Username");
        usernameL.setBounds(300, 20, 100, 20);
        add(usernameL);
        passwordL=new JLabel("Password");
        passwordL.setBounds(300, 60, 100, 20);
        add(passwordL);
        loginAsL=new JLabel("Logging in as");
        loginAsL.setBounds(300, 100, 100, 20);
        add(loginAsL);


        usernameJTF=new JTextField();
        usernameJTF.setBounds(400, 20, 150, 20);
        add(usernameJTF);
        passwordJF=new JPasswordField();
        passwordJF.setBounds(400, 60, 150, 20);
        add(passwordJF);

        setSize(640,300);
        setLocation(600,300);
        setVisible(true);

        loggingChoice=new Choice();
        loggingChoice.add("Admin");
        loggingChoice.add("Customer");
        loggingChoice.setBounds(400,100,150,20);
        add(loggingChoice);

    }






    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(actionEvent.getSource()==loginBtn){
            try {
                Conn conn=new Conn();
                String username=usernameJTF.getText();
                String password=passwordJF.getText();
                String user=loggingChoice.getSelectedItem();
                String query="SELECT * FROM login WHERE username=  '"+username+"' and password = '"+password+"'" +
                        "and user='"+user+"'";
                ResultSet resultSet=conn.statement.executeQuery(query);
                if(resultSet.next()){
                    String meter=resultSet.getString("meter_no");
                    new Project(meter,user).setVisible(true);
                    this.setVisible(false);
                }else{
                    JOptionPane.showMessageDialog(null,"Invalid login");
                    usernameJTF.setText("");
                    passwordJF.setText("");
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } else if (actionEvent.getSource()==cancelBtn) {
            this.setVisible(false);
        } else if (actionEvent.getSource()==signupBtn) {
            new Signup().setVisible(true);
        }
    }

    public static void main(String[] args) {
        new Login().setVisible(true);
    }
}
