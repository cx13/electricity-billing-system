package electricity;

import javax.swing.*;
import javax.swing.border.LineBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Signup extends JFrame implements ActionListener {
    JLabel usernameL,nameL,passwordL,createAccountL;
    JTextField usernameF,nameF,meterF;
    JPasswordField passwordF;
    Choice choiceAccounts;

    JButton createBtn,backBtn;

    JPanel panel;

    public Signup(){

        setBounds(600, 250, 700, 400);

        panel = new JPanel();
        panel.setBounds(30, 30, 650, 300);
        panel.setLayout( null);
        panel.setBackground(Color.WHITE);
        panel.setForeground(new Color(34, 139, 34));
        panel.setBorder(new TitledBorder(new LineBorder(new Color(173, 216, 230), 2), "Create-Account", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(173, 216, 230)));
        add(panel);

        //username
        usernameL=new JLabel("Username");
        usernameL.setBounds(100, 50, 100, 20);
        usernameF=new JTextField();
        usernameF.setBounds(260, 50, 150, 20);
        panel.add(usernameF);
        panel.add(usernameL);
        //name
        nameL=new JLabel("Name");
        nameL.setBounds(100, 90, 100, 20);
        nameF=new JTextField();
        nameF.setBounds(260, 90, 150, 20);
        panel.add(nameL);
        panel.add(nameF);

        //password
        passwordL=new JLabel("Password");
        passwordL.setBounds(100, 130, 100, 20);
        passwordF=new JPasswordField();
        passwordF.setBounds(260, 130, 150, 20);
        panel.add(passwordL);
        panel.add(passwordF);

        //create accounts as
        createAccountL=new JLabel("Create Account As");
        createAccountL.setBounds(100, 170, 140, 20);
        choiceAccounts=new Choice();
        choiceAccounts.add("Admin");
        choiceAccounts.add("Customer");
        choiceAccounts.setBounds(260, 170, 150, 20);
        panel.add(createAccountL);
        panel.add(choiceAccounts);

        //meter
        JLabel meterL = new JLabel("Meter Number");
        meterL.setForeground(Color.DARK_GRAY);
        meterL.setFont(new Font("Tahoma", Font.BOLD, 14));
        meterL.setBounds(100, 210, 100, 20);
        meterL.setVisible(false);
        panel.add(meterL);
        meterF = new JTextField();
        meterF.setBounds(260, 210, 150, 20);
        meterF.setVisible(false);
        panel.add(meterF);

        //button
        createBtn=new JButton("Create");
        createBtn.setBackground(Color.BLACK);
        createBtn.setForeground(Color.WHITE);
        createBtn.setBounds(140, 290, 120, 30);
        createBtn.addActionListener(this);
        panel.add(createBtn);

        backBtn=new JButton("Back");
        backBtn.setBackground(Color.BLACK);
        backBtn.setForeground(Color.WHITE);
        backBtn.addActionListener(this);
        backBtn.setBounds(300, 290, 120, 30);
        panel.add(backBtn);

        //sign up icon
        ImageIcon icon=new ImageIcon(ClassLoader.getSystemResource("icon/signupImage.png"));
        Image image=icon.getImage().getScaledInstance(250, 250, Image.SCALE_DEFAULT);
        ImageIcon img=new ImageIcon(image);
        JLabel label=new JLabel(img);
        label.setBounds(450, 30, 250, 250);
        panel.add(label);

        choiceAccounts.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent itemEvent) {
                String user=choiceAccounts.getSelectedItem();
                if(user.equals("Customer")){
                    meterL.setVisible(true);
                    meterF.setVisible(true);
                }else{
                    meterL.setVisible(false);
                    meterF.setVisible(false);
                }
            }
        });



    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        if(actionEvent.getSource()==createBtn){
            Conn conn=new Conn();
            String username=usernameF.getText();
            String name=nameF.getText();
            String password=passwordF.getText();
            String user=choiceAccounts.getSelectedItem();
            String meter=meterF.getText();
            try {
                String query=null;
                if(user.equals("Admin")) {
                    query = "insert into login values('" + meter + "', '" + username + "', '" + name + "', '" + password + "', '" + user + "')";
                }else {
                    query="update login set username = '"+username+"', name = '"+name+"', password = '"+password+"', user = '"+user+"' where meter_no = '"+meterF.getText()+"'";
                }
                conn.statement.executeQuery(query);
                JOptionPane.showMessageDialog(null,"Account Created Successfully");
                this.setVisible(false);
                new Login().setVisible(true);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        } else if (actionEvent.getSource()==backBtn) {
            this.setVisible(false);
            new Login().setVisible(true);
        }

    }

    public static void main(String[] args) {
        new Signup().setVisible(true);
    }
}
